﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Queries
{
    public static class ProjectQueries
    {
        public static IQueryable<Project> WhereUserHasPermission(this IQueryable<Project> query, User user)
        {
            if (user.Role.Equals(Roles.Admin))
            {
                return query;
            }
            return query.Where(x => x.Permissions.Any(z => !x.IsDeleted && x.IsActive && z.UserId.Equals(user.Id)));
        }

        public static bool HasOwnerPermission(this Project project, User user)
        {
            if (user.Role.Equals(Roles.Admin))
            {
                return true;
            }
            return project.Permissions.Any(x => !x.IsDeleted && x.IsActive && x.UserId.Equals(user.Id) && x.Permission.Equals(Permissions.Owner));
        }

        public static bool HasPermission(this Project project, User user)
        {
            if (user.Role.Equals(Roles.Admin))
            {
                return true;
            }
            return project.Permissions.Any(x => !x.IsDeleted && x.IsActive && x.UserId.Equals(user.Id));
        }
    }
}
