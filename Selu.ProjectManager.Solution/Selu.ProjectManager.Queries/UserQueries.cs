﻿using System.Linq;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Queries
{
    public static class UserQueries
    {
        public static User GetByEmail(this IQueryable<User> query, string email)
        {
            return query.FirstOrDefault(x => x.Email.Equals(email));
        }

		public static IQueryable<User> GetByRole(this IQueryable<User> query, string roleName)
		{
			return query.Where(x => x.Role.Contains(roleName));
		}
    }
}
