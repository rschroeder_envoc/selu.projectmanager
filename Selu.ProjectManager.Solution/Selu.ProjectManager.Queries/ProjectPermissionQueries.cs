﻿using System.Linq;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Queries
{
	public static class ProjectPermissionQueries
	{
		public static IQueryable<User> GetUsersByProject(this IQueryable<ProjectPermission> query, int projectId)
		{
			return query.Where(x => x.ProjectId.Equals(projectId)).Select(x => x.User);
		}
	}
}
