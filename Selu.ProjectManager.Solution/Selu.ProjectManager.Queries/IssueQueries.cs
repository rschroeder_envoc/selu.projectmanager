﻿using System.Linq;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Queries
{
    public static class IssueQueries
    {
        public static IQueryable<Issue> GetByProjectId(this IQueryable<Issue> query, int projectId)
        {
            return query.Where(x => x.ProjectId.Equals(projectId));
        }
    }
}
