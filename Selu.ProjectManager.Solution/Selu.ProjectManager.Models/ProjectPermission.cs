﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Selu.ProjectManager.Models
{
    public class ProjectPermission : IIdentifiable, IAuditable
    {
        public ProjectPermission()
        {
            IsActive = true;
        }

        public int Id { get; set; }

        public int ProjectId { get; set; }

        public virtual Project Project { get; set; }
        
        public int UserId { get; set; }

        public virtual User User { get; set; }

        public string Permission { get; set; }

        public DateTime CreatedDateUtc { get; set; }
        
        public DateTime UpdatedDateUtc { get; set; }
        
        public bool IsActive { get; set; }
        
        public bool IsDeleted { get; set; }
    }
}
