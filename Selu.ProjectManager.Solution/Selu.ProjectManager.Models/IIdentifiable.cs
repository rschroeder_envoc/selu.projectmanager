namespace Selu.ProjectManager.Models
{
    public interface IIdentifiable
    {
        int Id { get; set; }
    }
}