﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Selu.ProjectManager.Models
{
    public class AccessLog : IIdentifiable, IAuditable
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public virtual User User { get; set; }

        public DateTime CreatedDateUtc { get; set; }

        public DateTime UpdatedDateUtc { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }
    }
}
