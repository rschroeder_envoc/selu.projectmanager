﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Selu.ProjectManager.Models
{
    public static class Permissions
    {
        public static string[] GetAll { get { return new[] { Developer, Owner }; } }

        public static string Owner { get { return "Owner"; } }

        public static string Developer { get { return "Developer"; } }
    }
}
