using System;

namespace Selu.ProjectManager.Models
{
    public interface IAuditable
    {
        DateTime CreatedDateUtc { get; set; }
        
        DateTime UpdatedDateUtc { get; set; }

        bool IsActive { get; set; }

        bool IsDeleted { get; set; }
    }
}