﻿using System;

namespace Selu.ProjectManager.Models
{
    public class TimeUnitType : IIdentifiable, IAuditable
    {
        public TimeUnitType()
        {
            IsActive = true;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreatedDateUtc { get; set; }

        public DateTime UpdatedDateUtc { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }
    }
}
