﻿using System;

namespace Selu.ProjectManager.Models
{
	public class IssueStatus : IIdentifiable, IAuditable
	{
		public IssueStatus()
        {
            IsActive = true;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreatedDateUtc { get; set; }

        public DateTime UpdatedDateUtc { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }
	}
}
