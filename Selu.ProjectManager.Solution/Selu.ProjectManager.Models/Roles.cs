﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Selu.ProjectManager.Models
{
    public static class Roles
    {
        public static string[] AllRoles
        {
            get { return new[] { Admin, User }; }
        }

        public static string Admin
        {
            get { return "Admin"; }
        }

        public static string User
        {
            get { return "User"; }
        }
    }
}
