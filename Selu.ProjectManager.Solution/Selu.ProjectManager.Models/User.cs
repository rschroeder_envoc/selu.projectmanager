﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Selu.ProjectManager.Models
{
    public class User : IIdentifiable, IAuditable
    {
        public User()
        {
            IsActive = true;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string Role { get; set; }

        public DateTime CreatedDateUtc { get; set; }

        public DateTime UpdatedDateUtc { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        public ICollection<AccessLog> AccessLogs { get; set; }
    }
}
