﻿using System;

namespace Selu.ProjectManager.Models
{
    public class WorkLog : IIdentifiable, IAuditable
    {
        public WorkLog()
        {
            IsActive = true;
        }

        public int UserId { get; set; }

        public int IssueId { get; set; }

        public DateTime DateWorked { get; set; }

        public double HoursWorked { get; set; }

        public string Description { get; set; }

        public int Id { get; set; }

        public DateTime CreatedDateUtc { get; set; }

        public DateTime UpdatedDateUtc { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        public virtual User User { get; set; }

        public virtual Issue Issue { get; set; }
    }
}
