﻿using System;
using System.Collections.Generic;

namespace Selu.ProjectManager.Models
{
    public class Issue : IIdentifiable, IAuditable
    {
        public Issue()
        {
            IsActive = true;
        }

        public int Id { get; set; }

    	public int AssignedToUserId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int ProjectId { get; set; }

        public int IssueTypeId { get; set; }
        
        public int IssuePriorityId { get; set; }

		public int IssueStatusId { get; set; }

        public double EstimatedTime { get; set; }

        public int EstimatedTimeUnitTypeId { get; set; }

        public DateTime DeadlineDate { get; set; }

        public DateTime CreatedDateUtc { get; set; }

        public DateTime UpdatedDateUtc { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        public virtual ICollection<WorkLog> WorkLogs { get; set; }

        public virtual Project Project { get; set; }

		public virtual User AssignedToUser { get; set; }

        public virtual IssueType IssueType { get; set; }

        public virtual IssuePriority IssuePriority { get; set; }

		public virtual IssueStatus IssueStatus { get; set; }

        public virtual TimeUnitType EstimatedTimeUnitType { get; set; }
    }
}
