﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Selu.ProjectManager.Models
{
    public class Project : IIdentifiable, IAuditable
    {
        public Project()
        {
            IsActive = true;
            Issues = new List<Issue>();
            Permissions = new List<ProjectPermission>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDateUtc { get; set; }

        public DateTime UpdatedDateUtc { get; set; }

        public bool IsDeleted { get; set; }

        public virtual ICollection<Issue> Issues { get; private set; }

        public virtual ICollection<ProjectPermission> Permissions { get; private set; }

        public double HoursWorked
        {
            get { return Issues.Where(x => !x.IsDeleted).Sum(x => x.WorkLogs.Where(z => !z.IsDeleted).Sum(y => y.HoursWorked)); }
        }
    }
}
