﻿using System.Web;
using System.Web.Mvc;
using MvcMiniProfiler;
using Selu.ProjectManager.Web.App_Start;

namespace Selu.ProjectManager.Web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalFilterConfiguration.Configure();
            AreaRegistration.RegisterAllAreas();
            RouteConfiguration.Configure();
            BundleConfiguration.Configure();
            ValidationConfiguration.Configure();
            AutoMapperRegistrar.Configure();
            AutoFacConfiguration.Configure();
            MiniProfilerEF.Initialize();
            DatabaseUpgrader.PerformUpgrade();
        }
    }
}