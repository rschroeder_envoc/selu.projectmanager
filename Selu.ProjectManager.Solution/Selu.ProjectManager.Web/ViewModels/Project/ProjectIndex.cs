﻿namespace Selu.ProjectManager.Web.ViewModels.Project
{
    public class ProjectIndex
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public double HoursWorked { get; set; }

        public bool CanEdit { get; set; }
    }
}