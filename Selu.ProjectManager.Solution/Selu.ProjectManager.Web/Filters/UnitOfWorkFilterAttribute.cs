﻿using System.Web.Mvc;
using Selu.ProjectManager.Common.Data;
using Selu.ProjectManager.Common.Extentions;

namespace Selu.ProjectManager.Web.Filters
{
    public class UnitOfWorkFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var unitOfWork = DependencyResolver.Current.GetService<IUnitOfWork>();
            unitOfWork.Begin();
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var unitOfWork = DependencyResolver.Current.GetService<IUnitOfWork>();
            if (filterContext.Exception.IsNotNull())
            {
                unitOfWork.Rollback();
                return;
            }
            try
            {
                unitOfWork.Commit();
            }
            catch
            {
                unitOfWork.Rollback();
                throw;
            }
            finally
            {
                unitOfWork.Dispose();
            }
        }
    }
}