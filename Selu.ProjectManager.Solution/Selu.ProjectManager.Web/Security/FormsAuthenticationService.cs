using System;
using System.Web;
using System.Web.Security;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Web.Security
{
    public class FormsAuthenticationService : IFormsAuthenticationService
    {
        public void SignIn(User user)
        {
            HttpContext context = HttpContext.Current;

            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                     version: 1,
                     name: user.Email,
                     issueDate: DateTime.UtcNow,
                     expiration: DateTime.UtcNow.AddMinutes(30),
                     isPersistent: false,
                     userData: user.Role
            );

            string encryptedTicket = FormsAuthentication.Encrypt(ticket);
            var formsCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            context.Response.Cookies.Add(formsCookie);
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }

        public string GetRedirectUrl()
        {
            return FormsAuthentication.GetRedirectUrl("", false);
        }
    }
}