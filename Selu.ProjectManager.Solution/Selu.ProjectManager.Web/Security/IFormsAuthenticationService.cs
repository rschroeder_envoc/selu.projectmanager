﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Web.Security
{
    public interface IFormsAuthenticationService
    {
        void SignIn(User user);

        void SignOut();

        string GetRedirectUrl();
    }
}