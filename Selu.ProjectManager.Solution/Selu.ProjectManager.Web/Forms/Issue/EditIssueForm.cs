﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Web.Forms.Issue
{
    public class EditIssueForm
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        [DisplayName("Issue Type")]
        public int IssueTypeId { get; set; }

		[DisplayName("Status")]
		public int IssueStatusId { get; set; }

        [DisplayName("Prority")]
        public int IssuePriorityId { get; set; }

		[DisplayName("Assigned To")]
		public int AssignedToUserId { get; set; }

        public double EstimatedTime { get; set; }

        public int EstimatedTimeUnitTypeId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        [DisplayName("Deadline")]
        public DateTime DeadlineDate { get; set; }
        
        public List<IssueType> IssueTypes { get; set; }

        public List<IssuePriority> IssuePriorities { get; set; }

		public List<IssueStatus> IssueStatuses { get; set; }

        public List<TimeUnitType> TimeUnitTypes { get; set; }

		public List<User> Users { get; set; }
    }
}