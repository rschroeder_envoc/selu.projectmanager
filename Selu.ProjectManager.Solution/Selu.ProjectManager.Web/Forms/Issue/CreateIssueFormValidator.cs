using FluentValidation;

namespace Selu.ProjectManager.Web.Forms.Issue
{
    public class CreateIssueFormValidator : AbstractValidator<CreateIssueForm>
    {
        public CreateIssueFormValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .Length(0, 50);

            RuleFor(x => x.Description)
                .NotEmpty();

            RuleFor(x => x.EstimatedTime)
                .GreaterThan(0);
        }
    }
}