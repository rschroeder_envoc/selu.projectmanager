using FluentValidation;

namespace Selu.ProjectManager.Web.Forms.Issue
{
    public class EditIssueFormValidator : AbstractValidator<EditIssueForm>
    {
        public EditIssueFormValidator()
        {
            RuleFor(x => x.Id)
                .GreaterThan(0);

            RuleFor(x => x.ProjectId)
                .GreaterThan(0);

            RuleFor(x => x.Name)
                .NotEmpty();

            RuleFor(x => x.Description)
                .NotEmpty();

            RuleFor(x => x.EstimatedTime)
                .GreaterThan(0);
        }
    }
}