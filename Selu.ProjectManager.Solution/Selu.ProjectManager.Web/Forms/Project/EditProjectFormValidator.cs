using FluentValidation;

namespace Selu.ProjectManager.Web.Forms.Project
{
    public class EditProjectFormValidator : AbstractValidator<EditProjectForm>
    {
        public EditProjectFormValidator()
        {
            RuleFor(x => x.Id)
                .GreaterThan(0);

            RuleFor(x => x.Name)
                .NotEmpty()
                .Length(0, 50);
        }
    }
}