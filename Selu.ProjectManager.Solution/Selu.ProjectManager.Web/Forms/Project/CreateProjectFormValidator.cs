using FluentValidation;

namespace Selu.ProjectManager.Web.Forms.Project
{
    public class CreateProjectFormValidator : AbstractValidator<CreateProjectForm>
    {
        public CreateProjectFormValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .Length(0, 50);
        }
    }
}