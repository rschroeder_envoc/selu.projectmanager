﻿namespace Selu.ProjectManager.Web.Forms.Project
{
    public class EditProjectForm
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
    }
}