﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace Selu.ProjectManager.Web.Forms.ProjectPermission
{
    public class ProjectPermissionForm
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        [DisplayName("User")]
        public int UserId { get; set; }

        public string Permission { get; set; }

        public Models.Project Project { get; set; }

        public IEnumerable<Models.User> Users { get; set; }

        public SelectList UserSelectList
        {
            get
            {
                return new SelectList(Users, "Id", "Name");
            }
        }

        public SelectList PermissionSelectList
        {
            get
            {
                return new SelectList(Models.Permissions.GetAll);
            }
        }
    }
}