﻿using FluentValidation;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Web.Forms.ProjectPermission
{
    public class ProjectPermissionFormValidator : AbstractValidator<ProjectPermissionForm>
    {
        public ProjectPermissionFormValidator()
        {
            RuleFor(x => x.UserId)
                .NotEmpty()
                .WithMessage("User is required.");

            RuleFor(x => x.Permission)
                .NotEmpty()
                .Matches(string.Join("|", Permissions.GetAll))
                .WithMessage("Permission is required.");
        }
    }
}