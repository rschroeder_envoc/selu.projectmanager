﻿using System.ComponentModel.DataAnnotations;

namespace Selu.ProjectManager.Web.Forms.Authentication
{
    public class LoginForm
    {
        public string Email { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}