﻿using System;
using System.Linq;
using System.Web.Helpers;
using FluentValidation;
using FluentValidation.Results;
using Selu.ProjectManager.Common.Data;
using Selu.ProjectManager.Common.Extentions;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Web.Forms.Authentication
{
    public class LoginFormValidator : AbstractValidator<LoginForm>
    {
        private readonly IRepository<User> repository;

        public LoginFormValidator(IRepository<User> repository)
        {
            this.repository = repository;
            RuleFor(x => x.Email)
                .NotEmpty();

            RuleFor(x => x.Password)
                .NotEmpty();

            Custom(ValidateCredentials());
        }

        private Func<LoginForm, ValidationFailure> ValidateCredentials()
        {
            return (form) =>
                       {
                           var user = repository.Select().FirstOrDefault(x => x.Email.Equals(form.Email));
                           if (user.IsNull())
                           {
                               return new ValidationFailure("Password", "Invalid email or password.");
                           }
                           if (!user.IsActive)
                           {
                               return new ValidationFailure("Email", "Your account has been deactivated.");
                           }
                           if (!Crypto.VerifyHashedPassword(user.Password, form.Password))
                           {
                               return new ValidationFailure("Password", "Invalid username or password.");
                           }
                           return null;
                       };
        }
    }
}