using FluentValidation;

namespace Selu.ProjectManager.Web.Forms.WorkLog
{
    public class EditWorkLogFormValidator : AbstractValidator<EditWorkLogForm>
    {
        public EditWorkLogFormValidator()
        {
            RuleFor(x => x.Id)
                .GreaterThan(0);

            RuleFor(x => x.IssueId)
                .GreaterThan(0);

            RuleFor(x => x.HoursWorked)
                .GreaterThan(0);

            RuleFor(x => x.HoursWorked)
                .GreaterThan(0);

            RuleFor(x => x.Description)
                .NotEmpty();
        }
    }
}