using FluentValidation;

namespace Selu.ProjectManager.Web.Forms.WorkLog
{
    public class CreateWorkLogFormValidator : AbstractValidator<CreateWorkLogForm>
    {
        public CreateWorkLogFormValidator()
        {
            RuleFor(x => x.IssueId)
                .GreaterThan(0);

            RuleFor(x => x.HoursWorked)
                .GreaterThan(0);

            RuleFor(x => x.Description)
                .NotEmpty();
        }
    }
}