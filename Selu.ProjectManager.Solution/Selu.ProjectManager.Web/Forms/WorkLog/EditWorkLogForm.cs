﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Web.Forms.WorkLog
{
    public class EditWorkLogForm
    {
        public int Id { get; set; }

        public int IssueId { get; set; }

        [DisplayName("User")]
        public int UserId { get; set; }

        [DisplayName("Date Worked")]
        public DateTime DateWorked { get; set; }

        [DisplayName("Hours Worked")]
        public double HoursWorked { get; set; }

        public string Description { get; set; }

        public List<User> Users { get; set; }
    }
}