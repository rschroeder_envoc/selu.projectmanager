﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Selu.ProjectManager.Common.Data;
using Selu.ProjectManager.Common.Extentions;
using Selu.ProjectManager.Models;
using Selu.ProjectManager.Web.Areas.Admin.Models.IssuePriority;

namespace Selu.ProjectManager.Web.Areas.Admin.Controllers
{
    public class IssuePriorityController : AdminController
    {
        private readonly IRepository<IssuePriority> repository;

        public IssuePriorityController(IRepository<IssuePriority> repository)
        {
            this.repository = repository;
        }

        public ActionResult Index()
        {
            return View(repository.Select().ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CreateIssuePriorityForm form)
        {
            if (ModelState.IsValid)
            {
                var issuePriority = Mapper.Map<CreateIssuePriorityForm, IssuePriority>(form);
                repository.InsertOnCommit(issuePriority);
                return RedirectToAction("Index");
            }

            return View(form);
        }

        public ActionResult Edit(int id)
        {
            var issuePriority = repository.GetByIdWith(id);
            if (issuePriority.IsNull())
            {
                return HttpNotFound();
            }

            var form = Mapper.Map<IssuePriority, EditIssuePriorityForm>(issuePriority);

            return View(form);
        }

        [HttpPost]
        public ActionResult Edit(EditIssuePriorityForm form)
        {
            if (ModelState.IsValid)
            {
                var issuePriority = repository.GetById(form.Id);
                Mapper.Map(form, issuePriority);

                return RedirectToAction("Index");
            }

            return View(form);
        }

        public ActionResult Delete(int id)
        {
            var issuePriority = repository.GetById(id);
            if (issuePriority.IsNull())
            {
                return HttpNotFound();
            }

            return View(issuePriority);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult PerformDelete(int id)
        {
            var issuePriority = repository.GetById(id);
            if (issuePriority.IsNull())
            {
                return HttpNotFound();
            }

            issuePriority.IsDeleted = true;

            return RedirectToAction("Index");
        }
    }
}