﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Selu.ProjectManager.Common.Data;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Web.Areas.Admin.Controllers
{
    public class AccessLogController : AdminController
    {
        private readonly IRepository<User> repository;

        public AccessLogController(IRepository<User> repository)
        {
            this.repository = repository;
        }

        public ActionResult Index()
        {
            return View(repository.SelectWith(x => x.AccessLogs).ToList());
        }
    }
}
