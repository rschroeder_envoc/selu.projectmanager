﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Selu.ProjectManager.Common.Data;
using Selu.ProjectManager.Common.Extentions;
using Selu.ProjectManager.Models;
using Selu.ProjectManager.Web.Areas.Admin.Models.IssueType;

namespace Selu.ProjectManager.Web.Areas.Admin.Controllers
{
    public class IssueTypeController : AdminController
    {
        private readonly IRepository<IssueType> repository;

        public IssueTypeController(IRepository<IssueType> repository)
        {
            this.repository = repository;
        }

        public ActionResult Index()
        {
            return View(repository.Select().ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CreateIssueTypeForm form)
        {
            if (ModelState.IsValid)
            {
                var issueType = Mapper.Map<CreateIssueTypeForm, IssueType>(form);
                repository.InsertOnCommit(issueType);
                return RedirectToAction("Index");
            }

            return View(form);
        }

        public ActionResult Edit(int id)
        {
            var issueType = repository.GetByIdWith(id);
            if (issueType.IsNull())
            {
                return HttpNotFound();
            }

            var form = Mapper.Map<IssueType, EditIssueTypeForm>(issueType);

            return View(form);
        }

        [HttpPost]
        public ActionResult Edit(EditIssueTypeForm form)
        {
            if (ModelState.IsValid)
            {
                var issueType = repository.GetById(form.Id);
                Mapper.Map(form, issueType);

                return RedirectToAction("Index");
            }

            return View(form);
        }

        public ActionResult Delete(int id)
        {
            var issueType = repository.GetById(id);
            if (issueType.IsNull())
            {
                return HttpNotFound();
            }

            return View(issueType);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult PerformDelete(int id)
        {
            var issueType = repository.GetById(id);
            if (issueType.IsNull())
            {
                return HttpNotFound();
            }

            issueType.IsDeleted = true;

            return RedirectToAction("Index");
        }
    }
}