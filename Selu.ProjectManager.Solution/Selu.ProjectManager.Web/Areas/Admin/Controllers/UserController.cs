﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using AutoMapper;
using Selu.ProjectManager.Common.Data;
using Selu.ProjectManager.Common.Extentions;
using Selu.ProjectManager.Models;
using Selu.ProjectManager.Web.Areas.Admin.Models.User;

namespace Selu.ProjectManager.Web.Areas.Admin.Controllers
{
    public class UserController : AdminController
    {
        private readonly IRepository<User> repository;

        public UserController(IRepository<User> repository)
        {
            this.repository = repository;
        }

        public ActionResult Index()
        {
            return View(repository.Select().ToList());
        }

        public ActionResult Create()
        {
            return View(new CreateUserForm());
        }

        [HttpPost]
        public ActionResult Create(CreateUserForm form)
        {
            if (!ModelState.IsValid)
            {
                return View(form);
            }
            var user = Mapper.Map<CreateUserForm, User>(form);
            user.Password = Crypto.HashPassword(user.Password);
            repository.InsertOnCommit(user);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var user = repository.GetById(id);
            if (user.IsNull())
            {
                return HttpNotFound();
            }
            var form = Mapper.Map<User, EditUserForm>(user);
            return View(form);
        }

        [HttpPost]
        public ActionResult Edit(EditUserForm form)
        {
            if (!ModelState.IsValid)
            {
                return View(form);
            }
            var user = repository.GetById(form.Id);
            Mapper.Map(form, user);
            return RedirectToAction("Index");
        }

        public ActionResult ResetPassword(int id)
        {
            var user = repository.GetById(id);
            if (user.IsNull())
            {
                return HttpNotFound();
            }
            var form = Mapper.Map<User, ResetPasswordForm>(user);
            return View(form);
        }

        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordForm form)
        {
            var user = repository.GetById(form.Id);
            if (user.IsNull())
            {
                return HttpNotFound();
            }
            user.Password = Crypto.HashPassword(form.NewPassword);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            var user = repository.GetById(id);
            if (user.IsNull())
            {
                return HttpNotFound();
            }
            return View(user);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var user = repository.GetById(id);
            user.IsDeleted = true;
            return RedirectToAction("Index");
        }
    }
}