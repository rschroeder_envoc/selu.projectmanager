﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Selu.ProjectManager.Common.Data;
using Selu.ProjectManager.Common.Extentions;
using Selu.ProjectManager.Models;
using Selu.ProjectManager.Web.Areas.Admin.Models.IssueStatus;

namespace Selu.ProjectManager.Web.Areas.Admin.Controllers
{
	public class IssueStatusController : AdminController
	{
		private readonly IRepository<IssueStatus> repository;

		public IssueStatusController(IRepository<IssueStatus> repository)
		{
			this.repository = repository;
		}

		public ActionResult Index()
		{
			return View(repository.Select().ToList());
		}

		public ActionResult Create()
		{
			return View();
		}

		[HttpPost]
		public ActionResult Create(CreateIssueStatusForm form)
		{
			if (ModelState.IsValid)
			{
				var issueStatus = Mapper.Map<CreateIssueStatusForm, IssueStatus>(form);
				repository.InsertOnCommit(issueStatus);
				return RedirectToAction("Index");
			}

			return View(form);
		}

		public ActionResult Edit(int id)
		{
			var issueStatus = repository.GetByIdWith(id);
			if (issueStatus.IsNull())
			{
				return HttpNotFound();
			}

			var form = Mapper.Map<IssueStatus, EditIssueStatusForm>(issueStatus);

			return View(form);
		}

		[HttpPost]
		public ActionResult Edit(EditIssueStatusForm form)
		{
			if (ModelState.IsValid)
			{
				var issueStatus = repository.GetById(form.Id);
				Mapper.Map(form, issueStatus);

				return RedirectToAction("Index");
			}

			return View(form);
		}

		public ActionResult Delete(int id)
		{
			var issueStatus = repository.GetById(id);
			if (issueStatus.IsNull())
			{
				return HttpNotFound();
			}

			return View(issueStatus);
		}

		[HttpPost, ActionName("Delete")]
		public ActionResult PerformDelete(int id)
		{
			var issueStatus = repository.GetById(id);
			if (issueStatus.IsNull())
			{
				return HttpNotFound();
			}

			issueStatus.IsDeleted = true;

			return RedirectToAction("Index");
		}
	}
}