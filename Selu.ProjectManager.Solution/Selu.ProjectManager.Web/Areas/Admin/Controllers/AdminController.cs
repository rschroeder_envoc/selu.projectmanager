﻿using System.Web.Mvc;

namespace Selu.ProjectManager.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
    }
}