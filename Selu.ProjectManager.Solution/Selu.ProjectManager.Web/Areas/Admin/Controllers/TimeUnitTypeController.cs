﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Selu.ProjectManager.Common.Data;
using Selu.ProjectManager.Common.Extentions;
using Selu.ProjectManager.Models;
using Selu.ProjectManager.Web.Areas.Admin.Models.TimeUnitType;

namespace Selu.ProjectManager.Web.Areas.Admin.Controllers
{
    public class TimeUnitTypeController : AdminController
    {
        private readonly IRepository<TimeUnitType> repository;

        public TimeUnitTypeController(IRepository<TimeUnitType> repository)
        {
            this.repository = repository;
        }

        public ActionResult Index()
        {
            return View(repository.Select().ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CreateTimeUnitTypeForm form)
        {
            if (ModelState.IsValid)
            {
                var TimeUnitType = Mapper.Map<CreateTimeUnitTypeForm, TimeUnitType>(form);
                repository.InsertOnCommit(TimeUnitType);
                return RedirectToAction("Index");
            }

            return View(form);
        }

        public ActionResult Edit(int id)
        {
            var TimeUnitType = repository.GetByIdWith(id);
            if (TimeUnitType.IsNull())
            {
                return HttpNotFound();
            }

            var form = Mapper.Map<TimeUnitType, EditTimeUnitTypeForm>(TimeUnitType);

            return View(form);
        }

        [HttpPost]
        public ActionResult Edit(EditTimeUnitTypeForm form)
        {
            if (ModelState.IsValid)
            {
                var TimeUnitType = repository.GetById(form.Id);
                Mapper.Map(form, TimeUnitType);

                return RedirectToAction("Index");
            }

            return View(form);
        }

        public ActionResult Delete(int id)
        {
            var TimeUnitType = repository.GetById(id);
            if (TimeUnitType.IsNull())
            {
                return HttpNotFound();
            }

            return View(TimeUnitType);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult PerformDelete(int id)
        {
            var TimeUnitType = repository.GetById(id);
            if (TimeUnitType.IsNull())
            {
                return HttpNotFound();
            }

            TimeUnitType.IsDeleted = true;

            return RedirectToAction("Index");
        }
    }
}
