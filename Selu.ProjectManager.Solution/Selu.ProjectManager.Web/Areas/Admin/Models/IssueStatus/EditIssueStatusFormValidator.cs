﻿using FluentValidation;

namespace Selu.ProjectManager.Web.Areas.Admin.Models.IssueStatus
{
    public class EditIssueStatusFormValidator : AbstractValidator<EditIssueStatusForm>
    {
        public EditIssueStatusFormValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .Length(0, 50);
        }
    }
}