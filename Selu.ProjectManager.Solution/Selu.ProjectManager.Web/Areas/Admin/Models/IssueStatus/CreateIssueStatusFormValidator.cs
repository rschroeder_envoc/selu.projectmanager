﻿using FluentValidation;

namespace Selu.ProjectManager.Web.Areas.Admin.Models.IssueStatus
{
    public class CreateIssueStatusFormValidator : AbstractValidator<CreateIssueStatusForm>
    {
        public CreateIssueStatusFormValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .Length(0, 50);
        }
    }
}