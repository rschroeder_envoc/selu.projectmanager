﻿namespace Selu.ProjectManager.Web.Areas.Admin.Models.IssueStatus
{
    public class CreateIssueStatusForm
    {
        public string Name { get; set; }
    }
}