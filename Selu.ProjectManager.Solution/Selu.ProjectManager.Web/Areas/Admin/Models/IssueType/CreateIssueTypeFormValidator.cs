﻿using FluentValidation;

namespace Selu.ProjectManager.Web.Areas.Admin.Models.IssueType
{
    public class CreateIssueTypeFormValidator : AbstractValidator<CreateIssueTypeForm>
    {
        public CreateIssueTypeFormValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .Length(0, 50);
        }
    }
}