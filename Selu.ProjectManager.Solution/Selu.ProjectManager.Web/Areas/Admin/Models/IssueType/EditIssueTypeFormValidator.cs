﻿using FluentValidation;

namespace Selu.ProjectManager.Web.Areas.Admin.Models.IssueType
{
    public class EditIssueTypeFormValidator : AbstractValidator<EditIssueTypeForm>
    {
        public EditIssueTypeFormValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .Length(0, 50);
        }
    }
}