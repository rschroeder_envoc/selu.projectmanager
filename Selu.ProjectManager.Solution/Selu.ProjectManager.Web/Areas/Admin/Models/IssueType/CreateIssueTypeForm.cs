﻿namespace Selu.ProjectManager.Web.Areas.Admin.Models.IssueType
{
    public class CreateIssueTypeForm
    {
        public string Name { get; set; }
    }
}