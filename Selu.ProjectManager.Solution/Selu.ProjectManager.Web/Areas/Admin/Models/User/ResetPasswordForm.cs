﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Selu.ProjectManager.Web.Areas.Admin.Models.User
{
    public class ResetPasswordForm
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        [DisplayName("New Password"), DataType(DataType.Password)]
        public string NewPassword { get; set; }
    }
}