﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Web.Areas.Admin.Models.User
{
    public class EditUserForm
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Role { get; set; }

        [DisplayName("Is Active?")]
        public bool IsActive { get; set; }

        public IEnumerable<SelectListItem> RoleSelectList
        {
            get
            {
                return Roles.AllRoles.Select(x => new SelectListItem
                {
                    Text = x,
                    Value = x
                });
            }
        }
    }
}