﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Web.Areas.Admin.Models.User
{
    public class CreateUserForm
    {
        public string Name { get; set; }

        public string Email { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [DisplayName("Confirm Password")]
        public string ConfirmPassword { get; set; }

        public string Role { get; set; }

        public IEnumerable<SelectListItem> RoleSelectList
        {
            get
            {
                return Roles.AllRoles.Select(x => new SelectListItem
                                                    {
                                                        Text = x,
                                                        Value = x
                                                    });
            }
        }
    }
}