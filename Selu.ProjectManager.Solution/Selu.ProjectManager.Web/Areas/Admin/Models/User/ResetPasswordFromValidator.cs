﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;

namespace Selu.ProjectManager.Web.Areas.Admin.Models.User
{
    public class ResetPasswordFromValidator : AbstractValidator<ResetPasswordForm>
    {
        public ResetPasswordFromValidator()
        {
            RuleFor(x => x.NewPassword)
                .NotEmpty()
                .Length(0, 50);
        }
    }
}