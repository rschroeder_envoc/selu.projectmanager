﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;
using Selu.ProjectManager.Common.Data;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Web.Areas.Admin.Models.User
{
    public class CreateUserFormValidator : AbstractValidator<CreateUserForm>
    {
        private readonly IRepository<ProjectManager.Models.User> repository;

        public CreateUserFormValidator(IRepository<ProjectManager.Models.User> repository)
        {
            this.repository = repository;

            RuleFor(x => x.Name)
                .NotEmpty()
                .Length(0, 50);

            RuleFor(x => x.Email)
                .NotEmpty()
                .EmailAddress()
                .Length(0, 50)
                .Must(BeAUniqueEmail())
                .WithMessage("This email address is already in use.");

            RuleFor(x => x.Password)
                .NotEmpty()
                .Length(0, 50);

            RuleFor(x => x.ConfirmPassword)
                .Equal(x => x.Password)
                .WithMessage("Confirm password must match password");

            RuleFor(x => x.Role)
                .NotEmpty()
                .Matches(string.Join("|", Roles.AllRoles));
        }

        private Func<string, bool> BeAUniqueEmail()
        {
            return email => !repository.Select().Any(x => x.Email.Equals(email));
        }
    }
}