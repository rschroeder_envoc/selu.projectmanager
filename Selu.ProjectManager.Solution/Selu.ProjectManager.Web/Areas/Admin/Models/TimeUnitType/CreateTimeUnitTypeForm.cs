﻿namespace Selu.ProjectManager.Web.Areas.Admin.Models.TimeUnitType
{
    public class CreateTimeUnitTypeForm
    {
        public string Name { get; set; }
    }
}