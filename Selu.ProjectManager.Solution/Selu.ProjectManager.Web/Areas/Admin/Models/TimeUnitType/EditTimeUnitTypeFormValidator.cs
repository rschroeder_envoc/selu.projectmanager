﻿using FluentValidation;

namespace Selu.ProjectManager.Web.Areas.Admin.Models.TimeUnitType
{
    public class EditTimeUnitTypeFormValidator : AbstractValidator<EditTimeUnitTypeForm>
    {
        public EditTimeUnitTypeFormValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .Length(0, 50);
        }
    }
}