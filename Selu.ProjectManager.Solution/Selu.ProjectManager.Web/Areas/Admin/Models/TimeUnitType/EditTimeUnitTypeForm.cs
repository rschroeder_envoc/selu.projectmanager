﻿using System.ComponentModel;

namespace Selu.ProjectManager.Web.Areas.Admin.Models.TimeUnitType
{
    public class EditTimeUnitTypeForm
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [DisplayName("Is Active?")]
        public bool IsActive { get; set; }
    }
}