﻿using FluentValidation;

namespace Selu.ProjectManager.Web.Areas.Admin.Models.TimeUnitType
{
    public class CreateTimeUnitTypeFormValidator : AbstractValidator<CreateTimeUnitTypeForm>
    {
        public CreateTimeUnitTypeFormValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .Length(0, 50);
        }
    }
}