﻿namespace Selu.ProjectManager.Web.Areas.Admin.Models.IssuePriority
{
    public class CreateIssuePriorityForm
    {
        public string Name { get; set; }
    }
}