﻿using FluentValidation;

namespace Selu.ProjectManager.Web.Areas.Admin.Models.IssuePriority
{
    public class CreateIssuePriorityFormValidator : AbstractValidator<CreateIssuePriorityForm>
    {
        public CreateIssuePriorityFormValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .Length(0, 50);
        }
    }
}