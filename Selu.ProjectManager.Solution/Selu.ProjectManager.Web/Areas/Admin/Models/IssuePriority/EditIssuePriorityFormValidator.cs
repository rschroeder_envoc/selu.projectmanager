﻿using FluentValidation;

namespace Selu.ProjectManager.Web.Areas.Admin.Models.IssuePriority
{
    public class EditIssuePriorityFormValidator : AbstractValidator<EditIssuePriorityForm>
    {
        public EditIssuePriorityFormValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .Length(0, 50);
        }
    }
}