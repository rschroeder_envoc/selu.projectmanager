﻿using System.Linq;
using System.Web.Mvc;
using Selu.ProjectManager.Common.Data;
using Selu.ProjectManager.Models;
using Selu.ProjectManager.Web.Forms.Authentication;
using Selu.ProjectManager.Web.Security;

namespace Selu.ProjectManager.Web.Controllers
{
    public class AuthenticationController : Controller
    {
        private readonly IRepository<User> repository;
        private readonly IFormsAuthenticationService authenticationService;
        private readonly IRepository<AccessLog> accessLogRepository;

        public AuthenticationController(IRepository<User> repository,
            IFormsAuthenticationService authenticationService,
            IRepository<AccessLog> accessLogRepository)
        {
            this.repository = repository;
            this.authenticationService = authenticationService;
            this.accessLogRepository = accessLogRepository;
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginForm form)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            var user = repository.Select().FirstOrDefault(x => x.Email.Equals(form.Email));
            authenticationService.SignIn(user);
            accessLogRepository.InsertOnCommit(new AccessLog { UserId = user.Id });
            return Redirect(authenticationService.GetRedirectUrl());
        }

        public ActionResult Logout()
        {
            authenticationService.SignOut();
            return RedirectToAction("Login");
        }
    }
}