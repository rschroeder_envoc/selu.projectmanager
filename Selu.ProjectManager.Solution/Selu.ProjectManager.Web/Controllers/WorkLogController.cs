﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Selu.ProjectManager.Common.Data;
using Selu.ProjectManager.Common.Extentions;
using Selu.ProjectManager.Models;
using Selu.ProjectManager.Web.Forms.WorkLog;

namespace Selu.ProjectManager.Web.Controllers
{
    public class WorkLogController : SecureController
    {
        private readonly IRepository<WorkLog> repository;
        private readonly IRepository<Issue> issueRepository;
        private readonly IRepository<User> userRepository;

        public WorkLogController(IRepository<WorkLog> repository,
            IRepository<Issue> issueRepository,
            IRepository<User> userRepository) :
            base(userRepository)
        {
            this.repository = repository;
            this.issueRepository = issueRepository;
            this.userRepository = userRepository;
        }

        public ActionResult Index(int id)
        {
            var issue = issueRepository.GetByIdWith(id,
                x => x.WorkLogs,
                x => x.WorkLogs.Select(z => z.User),
                x => x.WorkLogs.Select(z => z.Issue));
            var result = VerifyPermission(issue);
            if (result.IsNotNull())
            {
                return result;
            }

            return View(issue);
        }

        public ActionResult Create(int id)
        {
            var issue = issueRepository.GetById(id);
            var result = VerifyPermission(issue);
            if (result.IsNotNull())
            {
                return result;
            }

            var workLog = new WorkLog { Issue = issue, IssueId = id };
            var form = Mapper.Map<WorkLog, CreateWorkLogForm>(workLog);
            form.Users = userRepository.Select().ToList();

            return View(form);
        }

        [HttpPost]
        public ActionResult Create(CreateWorkLogForm form)
        {
            var issue = issueRepository.GetById(form.IssueId);
            var result = VerifyPermission(issue);
            if (result.IsNotNull())
            {
                return result;
            }

            if (ModelState.IsValid)
            {
                var workLog = Mapper.Map<CreateWorkLogForm, WorkLog>(form);
                if(!User.Role.Equals(Roles.Admin))
                {
                    workLog.UserId = User.Id;
                }
                repository.InsertOnCommit(workLog);

                return RedirectToAction("Index", new { id = form.IssueId });
            }

            form.Users = userRepository.Select().ToList();

            return View(form);
        }

        public ActionResult Edit(int id)
        {
            var workLog = repository.GetById(id);
            var result = VerifyPermission(workLog);
            if (result.IsNotNull())
            {
                return result;
            }

            var form = Mapper.Map<WorkLog, EditWorkLogForm>(workLog);
            form.Users = userRepository.Select().ToList();

            return View(form);
        }

        [HttpPost]
        public ActionResult Edit(EditWorkLogForm form)
        {
            var workLog = repository.GetById(form.Id);
            var result = VerifyPermission(workLog);
            if (result.IsNotNull())
            {
                return result;
            }
            if (ModelState.IsValid)
            {
                Mapper.Map(form, workLog);
                if (!User.Role.Equals(Roles.Admin))
                {
                    workLog.UserId = User.Id;
                }
                return RedirectToAction("Index", new { id = form.IssueId });
            }

            form.Users = userRepository.Select().ToList();

            return View(form);
        }

        public ActionResult Delete(int id)
        {
            var workLog = repository.GetById(id);
            var result = VerifyPermission(workLog);
            if (result.IsNotNull())
            {
                return result;
            }

            return View(workLog);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult PerformDelete(int id)
        {
            var workLog = repository.GetById(id);
            var result = VerifyPermission(workLog);
            if (result.IsNotNull())
            {
                return result;
            }

            workLog.IsDeleted = true;

            return RedirectToAction("Index", new { id = workLog.IssueId });
        }
    }
}