﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Selu.ProjectManager.Common.Data;
using Selu.ProjectManager.Common.Extentions;
using Selu.ProjectManager.Models;
using Selu.ProjectManager.Web.Forms.ProjectPermission;

namespace Selu.ProjectManager.Web.Controllers
{
    public class ProjectPermissionController : SecureController
    {
        private readonly IRepository<ProjectPermission> repository;
        private readonly IRepository<User> userRepository;
        private readonly IRepository<Project> projectRepository;

        public ProjectPermissionController(IRepository<ProjectPermission> repository,
            IRepository<User> userRepository,
            IRepository<Project> projectRepository) :
            base(userRepository)
        {
            this.repository = repository;
            this.userRepository = userRepository;
            this.projectRepository = projectRepository;
        }

        public ActionResult Index(int id)
        {
            var project = projectRepository.GetByIdWith(id, x => x.Permissions, x => x.Permissions.Select(z => z.User));
            var result = VerifyOwnerPermission(project);
            if (result.IsNotNull())
            {
                return result;
            }
            return View(project);
        }

        public ActionResult Create(int id)
        {
            var project = projectRepository.GetById(id);
            var result = VerifyOwnerPermission(project);
            if (result.IsNotNull())
            {
                return result;
            }
            var form = new ProjectPermissionForm
                           {
                               ProjectId = id,
                               Project = project,
                               Users = userRepository.Select().Where(x => x.IsActive).ToList()
                           };
            return View(form);
        }

        [HttpPost]
        public ActionResult Create(ProjectPermissionForm form)
        {
            var project = projectRepository.GetById(form.ProjectId);
            var result = VerifyOwnerPermission(project);
            if (result.IsNotNull())
            {
                return result;
            }
            if (ModelState.IsValid)
            {
                var permission = Mapper.Map<ProjectPermissionForm, ProjectPermission>(form);
                repository.InsertOnCommit(permission);
                return RedirectToAction("Index", new { id = permission.ProjectId });
            }
            form.Project = project;
            form.Users = userRepository.Select().Where(x => x.IsActive).ToList();
            return View(form);
        }

        public ActionResult Edit(int id)
        {
            var permission = repository.GetByIdWith(id, x => x.Project, x => x.User);
            var result = VerifyOwnerPermission(permission);
            if (result.IsNotNull())
            {
                return result;
            }
            var form = Mapper.Map<ProjectPermission, ProjectPermissionForm>(permission);
            form.Users = userRepository.Select().Where(x => x.IsActive).ToList();
            return View(form);
        }

        [HttpPost]
        public ActionResult Edit(ProjectPermissionForm form)
        {
            var permission = repository.GetById(form.Id);
            var result = VerifyOwnerPermission(permission);
            if (result.IsNotNull())
            {
                return result;
            }
            if (ModelState.IsValid)
            {
                Mapper.Map(form, permission);
                return RedirectToAction("Index", new { id = permission.ProjectId });
            }
            form.Project = projectRepository.GetById(form.Id);
            form.Users = userRepository.Select().Where(x => x.IsActive).ToList();
            return View(form);
        }

        public ActionResult Delete(int id)
        {
            var permission = repository.GetByIdWith(id, x => x.Project, x => x.User);
            var result = VerifyOwnerPermission(permission);
            if (result.IsNotNull())
            {
                return result;
            }
            return View(permission);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var permission = repository.GetByIdWith(id);
            var result = VerifyOwnerPermission(permission);
            if (result.IsNotNull())
            {
                return result;
            }
            permission.IsDeleted = true;
            return RedirectToAction("Index", new { id = permission.ProjectId });
        }

    }
}