﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Selu.ProjectManager.Common.Data;
using Selu.ProjectManager.Common.Extentions;
using Selu.ProjectManager.Models;
using Selu.ProjectManager.Queries;
using Selu.ProjectManager.Web.Controllers.Parameters;
using Selu.ProjectManager.Web.Forms.Issue;

namespace Selu.ProjectManager.Web.Controllers
{
    public class IssueController : SecureController
    {
        private readonly IssueControllerParameters parameters;

        public IssueController(IssueControllerParameters parameters,
            IRepository<User> userRepository)
            : base(userRepository)
        {
            this.parameters = parameters;
        }

        public ActionResult Index(int id)
        {
            var project = parameters.projectRepository.GetByIdWith(id, 
                x => x.Issues,
                x => x.Issues.Select(z => z.WorkLogs), 
                x => x.Issues.Select(z => z.IssueType), 
                x => x.Issues.Select(z => z.IssuePriority),
				x => x.Issues.Select(z => z.IssueStatus),
				x => x.Issues.Select(z => z.AssignedToUser),
                x => x.Issues.Select(z => z.EstimatedTimeUnitType));
            var result = VerifyPermission(project);
            if (result.IsNotNull())
            {
                return result;
            }

            return View(project);
        }

        public ActionResult Details(int id)
        {
            var issue = parameters.repository.GetByIdWith(id,
                x => x.Project,
                x => x.IssueType,
                x => x.IssueStatus,
                x => x.IssuePriority,
                x => x.AssignedToUser,
                x => x.EstimatedTimeUnitType);
            var result = VerifyPermission(issue);
            if (result.IsNotNull())
            {
                return result;
            }

            return View(issue);
        }

        public ActionResult Create(int id)
        {
            var project = parameters.projectRepository.GetById(id);
            var result = VerifyPermission(project);
            if (result.IsNotNull())
            {
                return result;
            }

            var issue = new Issue { Project = project, ProjectId = id };
            var form = Mapper.Map<Issue, CreateIssueForm>(issue);
            SetCreateIssueFormProperties(form, id);

            return View(form);
        }

        [HttpPost]
        public ActionResult Create(CreateIssueForm form)
        {
            var project = parameters.projectRepository.GetById(form.ProjectId);
            var result = VerifyPermission(project);
            if (result.IsNotNull())
            {
                return result;
            }

            if (ModelState.IsValid)
            {
                var issue = Mapper.Map<CreateIssueForm, Issue>(form);
                parameters.repository.InsertOnCommit(issue);

                return RedirectToAction("Index", new { id = form.ProjectId });
            }

            SetCreateIssueFormProperties(form, form.ProjectId);

            return View(form);
        }

        private void SetCreateIssueFormProperties(CreateIssueForm form, int projectId)
        {
            form.IssueTypes = parameters.issueTypeRepository.Select().ToList();
            form.IssuePriorities = parameters.issuePriorityRepository.Select().ToList();
            form.IssueStatuses = parameters.issueStatusRepository.Select().ToList();
            form.TimeUnitTypes = parameters.timeUnitTypeRepository.Select().ToList();
            form.Users = GetProjectUsers(projectId).ToList();
        }

        private IEnumerable<User> GetProjectUsers(int projectId)
        {
            var users = parameters.projectPermissionRepository.Select().GetUsersByProject(projectId).ToList();
            users = users.Union(parameters.userRepository.Select().GetByRole(Roles.Admin)).ToList();

            return users;
        }

        public ActionResult Edit(int id)
        {
            var issue = parameters.repository.GetByIdWith(id,
                x => x.IssueType,
                x => x.IssueStatus,
                x => x.IssuePriority,
                x => x.AssignedToUser,
                x => x.EstimatedTimeUnitType);
            var result = VerifyPermission(issue);
            if (result.IsNotNull())
            {
                return result;
            }

            var form = Mapper.Map<Issue, EditIssueForm>(issue);
            SetEditIssueFormProperties(form, issue.ProjectId);

            return View(form);
        }

        [HttpPost]
        public ActionResult Edit(EditIssueForm form)
        {
            var issue = parameters.repository.GetById(form.Id);
            var result = VerifyPermission(issue);
            if (result.IsNotNull())
            {
                return result;
            }

            if (ModelState.IsValid)
            {
                Mapper.Map(form, issue);
                return RedirectToAction("Index", new { id = form.ProjectId });
            }

            SetEditIssueFormProperties(form, form.ProjectId);

            return View(form);
        }

        private void SetEditIssueFormProperties(EditIssueForm form, int projectId)
        {
            form.IssueTypes = parameters.issueTypeRepository.Select().ToList();
            form.IssuePriorities = parameters.issuePriorityRepository.Select().ToList();
            form.IssueStatuses = parameters.issueStatusRepository.Select().ToList();
            form.TimeUnitTypes = parameters.timeUnitTypeRepository.Select().ToList();
            form.Users = GetProjectUsers(projectId).ToList();
        }

        public ActionResult Delete(int id)
        {
            var issue = parameters.repository.GetById(id);
            var result = VerifyPermission(issue);
            if (result.IsNotNull())
            {
                return result;
            }

            return View(issue);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult PerformDelete(int id)
        {
            var issue = parameters.repository.GetById(id);
            var result = VerifyPermission(issue);
            if (result.IsNotNull())
            {
                return result;
            }

            issue.IsDeleted = true;

            return RedirectToAction("Index", new { id = issue.ProjectId });
        }
    }
}