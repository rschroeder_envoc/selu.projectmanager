﻿using Selu.ProjectManager.Common.Data;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Web.Controllers.Parameters
{
    public class IssueControllerParameters
    {
        public readonly IRepository<Project> projectRepository;
        public readonly IRepository<Issue> repository;
        public readonly IRepository<IssueType> issueTypeRepository;
        public readonly IRepository<IssuePriority> issuePriorityRepository;
        public readonly IRepository<TimeUnitType> timeUnitTypeRepository;
    	public readonly IRepository<User> userRepository;
    	public readonly IRepository<ProjectPermission> projectPermissionRepository;
    	public readonly IRepository<IssueStatus> issueStatusRepository;

    	public IssueControllerParameters(IRepository<Project> projectRepository,
            IRepository<Issue> repository,
            IRepository<IssueType> issueTypeRepository,
            IRepository<IssuePriority> issuePriorityRepository,
            IRepository<TimeUnitType> timeUnitTypeRepository,
			IRepository<User> userRepository, 
			IRepository<ProjectPermission> projectPermissionRepository,
			IRepository<IssueStatus> issueStatusRepository)
        {
            this.projectRepository = projectRepository;
    		this.projectPermissionRepository = projectPermissionRepository;
    		this.issueStatusRepository = issueStatusRepository;
    		this.repository = repository;
            this.issueTypeRepository = issueTypeRepository;
            this.issuePriorityRepository = issuePriorityRepository;
            this.timeUnitTypeRepository = timeUnitTypeRepository;
        	this.userRepository = userRepository;
        }
    }
}