﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Selu.ProjectManager.Common.Data;
using Selu.ProjectManager.Common.Extentions;
using Selu.ProjectManager.Models;
using Selu.ProjectManager.Queries;

namespace Selu.ProjectManager.Web.Controllers
{
    [Authorize(Roles = "User,Admin")]
    public class SecureController : Controller
    {
        protected IRepository<User> UserRepository;

        public SecureController(IRepository<User> userRepository)
        {
            UserRepository = userRepository;
        }

        private User user;
        protected new User User
        { 
            get
            {
                if (user.IsNotNull())
                {
                    return user;
                }
                var email = base.User.Identity.Name;
                user = UserRepository.Select().FirstOrDefault(x => x.Email.Equals(email));
                return user;
            }
        }

        protected ActionResult VerifyPermission(Project project)
        {
            if (project.IsNull())
            {
                return HttpNotFound();
            }
            bool hasPermission = project.HasPermission(User);
            if (!hasPermission)
            {
                return HttpNotFound();
            }
            return null;
        }

        protected ActionResult VerifyPermission(Issue issue)
        {
            if (issue.IsNull())
            {
                return HttpNotFound();
            }
            return VerifyPermission(issue.Project);
        }

        protected ActionResult VerifyPermission(WorkLog workLog)
        {
            if (workLog.IsNull())
            {
                return HttpNotFound();
            }
            if (!User.Role.Equals(Roles.Admin) && !workLog.UserId.Equals(User.Id))
            {
                return HttpNotFound();
            }
            return VerifyPermission(workLog.Issue);
        }

        protected ActionResult VerifyOwnerPermission(Project project)
        {
            if (project.IsNull())
            {
                return HttpNotFound();
            }
            bool hasPermission = project.HasOwnerPermission(User);
            if (!hasPermission)
            {
                return HttpNotFound();
            }
            return null;
        }

        protected ActionResult VerifyOwnerPermission(ProjectPermission projectPermission)
        {
            if (projectPermission.IsNull())
            {
                return HttpNotFound();
            }
            return VerifyOwnerPermission(projectPermission.Project);
        }
    }
}