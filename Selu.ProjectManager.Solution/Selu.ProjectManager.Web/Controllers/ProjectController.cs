﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Selu.ProjectManager.Common.Data;
using Selu.ProjectManager.Common.Extentions;
using Selu.ProjectManager.Models;
using Selu.ProjectManager.Web.Forms.Project;
using Selu.ProjectManager.Queries;
using Selu.ProjectManager.Web.ViewModels.Project;

namespace Selu.ProjectManager.Web.Controllers
{
    public class ProjectController : SecureController
    {
        private readonly IRepository<Project> repository;

        public ProjectController(IRepository<Project> repository,
            IRepository<User> userRepository)
            : base(userRepository)
        {
            this.repository = repository;
        }

        public ActionResult Index()
        {
            var projects = repository.SelectWith(
                    x => x.Permissions,
                    x => x.Issues,
                    x => x.Issues.Select(z => z.WorkLogs)
                ).WhereUserHasPermission(User).ToList();

            var viewModel = projects.Select(x => new ProjectIndex
                                                     {
                                                         Id = x.Id,
                                                         Name = x.Name,
                                                         HoursWorked = x.HoursWorked,
                                                         CanEdit = x.HasOwnerPermission(User)
                                                     }).ToList();
            return View(viewModel);
        }

        public ActionResult Create()
        {
            if (!HttpContext.User.IsInRole("Admin"))
            {
                return HttpNotFound();
            }
            return View();
        }

        [HttpPost]
        public ActionResult Create(CreateProjectForm form)
        {
            if (!HttpContext.User.IsInRole("Admin"))
            {
                return HttpNotFound();
            }
            if (ModelState.IsValid)
            {
                var project = Mapper.Map<CreateProjectForm, Project>(form);
                project.Permissions.Add(new ProjectPermission
                                            {
                                                UserId = User.Id,
                                                Permission = Permissions.Owner,
                                            });
                repository.InsertOnCommit(project);
                return RedirectToAction("Index");
            }
            return View(form);
        }

        public ActionResult Edit(int id)
        {
            var project = repository.GetById(id);
            var result = VerifyOwnerPermission(project);
            if (result.IsNotNull())
            {
                return result;
            }
            var form = Mapper.Map<Project, EditProjectForm>(project);
            return View(form);
        }

        [HttpPost]
        public ActionResult Edit(EditProjectForm form)
        {
            var project = repository.GetById(form.Id);
            var result = VerifyOwnerPermission(project);
            if (result.IsNotNull())
            {
                return result;
            }
            if (ModelState.IsValid)
            {
                Mapper.Map(form, project);
                return RedirectToAction("Index");
            }
            return View(form);
        }

        public ActionResult Delete(int id)
        {
            var project = repository.GetById(id);
            var result = VerifyOwnerPermission(project);
            if (result.IsNotNull())
            {
                return result;
            }
            return View(project);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult PerformDelete(int id)
        {
            var project = repository.GetById(id);
            var result = VerifyOwnerPermission(project);
            if (result.IsNotNull())
            {
                return result;
            }
            project.IsDeleted = true;
            return RedirectToAction("Index");
        }
    }
}