﻿function InitializeDatatable($table, sortCol, sorting) {
	$dataTable = $table.dataTable({
		"sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ records per page"
		},
		"aaSorting": [[sortCol != undefined ? sortCol : 1, sorting || 'asc']]
	});

	$("tfoot th").each(function (i) {
		if (!$(this).is(".filter-me")) return;
		this.innerHTML = fnCreateSelect($dataTable.fnGetColumnData(i));
		$('select', this).change(function () {
			$dataTable.fnFilter($(this).val(), i);
		});
	});
}

function fnCreateSelect(aData) {
	var r = '<select class="no-width"><option value=""></option>', i, iLen = aData.length;
	for (i = 0; i < iLen; i++) {
		r += '<option value="' + aData[i] + '">' + aData[i] + '</option>';
	}
	return r + '</select>';
}

$(function() {
    $("input[autofocus]").focus();
});