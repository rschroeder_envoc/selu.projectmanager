using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Features.ResolveAnything;
using Autofac.Integration.Mvc;
using FluentValidation;
using Selu.ProjectManager.Common.Data;
using Selu.ProjectManager.Data;
using Selu.ProjectManager.Web.Security;

namespace Selu.ProjectManager.Web.App_Start
{
    public static class AutoFacConfiguration
    {
        public static void Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<DataContext>()
                .InstancePerHttpRequest();

            builder.RegisterGeneric(typeof(SqlRepository<>))
                .As(typeof(IRepository<>));

            builder.RegisterType<UnitOfWork>()
                .As<IUnitOfWork>()
                .InstancePerHttpRequest();

            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .AsClosedTypesOf(typeof(IValidator<>))
                .AsImplementedInterfaces();

            builder.RegisterType<FormsAuthenticationService>()
                .As<IFormsAuthenticationService>();

            builder.RegisterSource(new AnyConcreteTypeNotAlreadyRegisteredSource());

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}