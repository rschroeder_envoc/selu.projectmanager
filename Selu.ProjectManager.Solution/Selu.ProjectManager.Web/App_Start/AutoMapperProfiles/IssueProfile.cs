﻿using AutoMapper;
using Selu.ProjectManager.Models;
using Selu.ProjectManager.Web.Forms.Issue;

namespace Selu.ProjectManager.Web.App_Start.AutoMapperProfiles
{
    public class IssueProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Issue, CreateIssueForm>();
            CreateMap<CreateIssueForm, Issue>()
                .ForMember(x => x.DeadlineDate, x => x.MapFrom(y => y.DeadlineDate.ToUniversalTime()));

            CreateMap<EditIssueForm, Issue>()
                .ForMember(x => x.DeadlineDate, x => x.MapFrom(y => y.DeadlineDate.ToUniversalTime()));
            CreateMap<Issue, EditIssueForm>();
        }
    }
}