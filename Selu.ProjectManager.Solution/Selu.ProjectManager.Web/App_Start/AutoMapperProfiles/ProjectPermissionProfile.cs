﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Selu.ProjectManager.Models;
using Selu.ProjectManager.Web.Forms.ProjectPermission;

namespace Selu.ProjectManager.Web.App_Start.AutoMapperProfiles
{
    public class ProjectPermissionProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ProjectPermissionForm, ProjectPermission>()
                .ForMember(x => x.User, x => x.Ignore())
                .ForMember(x => x.Project, x => x.Ignore());
            CreateMap<ProjectPermission, ProjectPermissionForm>();
        }
    }
}