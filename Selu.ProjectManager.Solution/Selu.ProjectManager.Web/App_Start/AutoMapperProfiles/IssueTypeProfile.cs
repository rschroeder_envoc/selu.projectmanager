﻿using AutoMapper;
using Selu.ProjectManager.Models;
using Selu.ProjectManager.Web.Areas.Admin.Models.IssueType;

namespace Selu.ProjectManager.Web.App_Start.AutoMapperProfiles
{
    public class IssueTypeProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<CreateIssueTypeForm, IssueType>();

            CreateMap<EditIssueTypeForm, IssueType>();
            CreateMap<IssueType, EditIssueTypeForm>();
        }
    }
}