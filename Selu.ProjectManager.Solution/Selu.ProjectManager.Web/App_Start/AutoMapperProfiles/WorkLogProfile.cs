﻿using AutoMapper;
using Selu.ProjectManager.Models;
using Selu.ProjectManager.Web.Forms.WorkLog;

namespace Selu.ProjectManager.Web.App_Start.AutoMapperProfiles
{
    public class WorkLogProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<WorkLog, CreateWorkLogForm>();
            CreateMap<CreateWorkLogForm, WorkLog>()
                .ForMember(x => x.DateWorked, x => x.MapFrom(y => y.DateWorked.ToUniversalTime()));

            CreateMap<WorkLog, EditWorkLogForm>();
            CreateMap<EditWorkLogForm, WorkLog>()
                .ForMember(x => x.DateWorked, x => x.MapFrom(y => y.DateWorked.ToUniversalTime()));
        }
    }
}