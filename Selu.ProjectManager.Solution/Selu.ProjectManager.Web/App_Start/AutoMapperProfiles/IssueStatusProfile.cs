﻿using AutoMapper;
using Selu.ProjectManager.Models;
using Selu.ProjectManager.Web.Areas.Admin.Models.IssueStatus;

namespace Selu.ProjectManager.Web.App_Start.AutoMapperProfiles
{
	public class IssueStatusProfile : Profile
	{
		protected override void Configure()
		{
			CreateMap<CreateIssueStatusForm, IssueStatus>();

			CreateMap<EditIssueStatusForm, IssueStatus>();
			CreateMap<IssueStatus, EditIssueStatusForm>();
		}
	}
}