﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Selu.ProjectManager.Models;
using Selu.ProjectManager.Web.Areas.Admin.Models.User;

namespace Selu.ProjectManager.Web.App_Start.AutoMapperProfiles
{
    public class UserProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<CreateUserForm, User>();
            CreateMap<EditUserForm, User>();
            CreateMap<User, EditUserForm>();
            CreateMap<User, ResetPasswordForm>();
        } 
    }
}