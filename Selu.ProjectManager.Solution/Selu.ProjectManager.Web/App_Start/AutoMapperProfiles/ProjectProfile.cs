using AutoMapper;
using Selu.ProjectManager.Models;
using Selu.ProjectManager.Web.Forms.Project;

namespace Selu.ProjectManager.Web.App_Start.AutoMapperProfiles
{
    public class ProjectProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<CreateProjectForm, Project>();

            CreateMap<EditProjectForm, Project>();
            CreateMap<Project, EditProjectForm>();
        }
    }
}