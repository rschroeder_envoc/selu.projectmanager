﻿using AutoMapper;
using Selu.ProjectManager.Models;
using Selu.ProjectManager.Web.Areas.Admin.Models.IssuePriority;

namespace Selu.ProjectManager.Web.App_Start.AutoMapperProfiles
{
    public class IssuePriorityProfile : Profile
    {
        protected override void  Configure()
        {
            CreateMap<CreateIssuePriorityForm, IssuePriority>();

            CreateMap<EditIssuePriorityForm, IssuePriority>();
            CreateMap<IssuePriority, EditIssuePriorityForm>();
        }
    }
}