﻿using AutoMapper;
using Selu.ProjectManager.Models;
using Selu.ProjectManager.Web.Areas.Admin.Models.TimeUnitType;

namespace Selu.ProjectManager.Web.App_Start.AutoMapperProfiles
{
    public class TimeUnitTypeProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<CreateTimeUnitTypeForm, TimeUnitType>();

            CreateMap<EditTimeUnitTypeForm, TimeUnitType>();
            CreateMap<TimeUnitType, EditTimeUnitTypeForm>();
        }
    }
}