using System.Web.Mvc;
using MvcMiniProfiler.MVCHelpers;
using Selu.ProjectManager.Web.Filters;

namespace Selu.ProjectManager.Web.App_Start
{
    public static class GlobalFilterConfiguration
    {
        public static void Configure()
        {
            GlobalFilters.Filters.Add(new UnitOfWorkFilterAttribute());
            GlobalFilters.Filters.Add(new ProfilingActionFilter());
        }
    }
}