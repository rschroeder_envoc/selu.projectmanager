﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Selu.ProjectManager.Web.App_Start
{
    public static class RouteConfiguration
    {
        public static void Configure()
        {
            RouteTable.Routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            RouteTable.Routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Project", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}