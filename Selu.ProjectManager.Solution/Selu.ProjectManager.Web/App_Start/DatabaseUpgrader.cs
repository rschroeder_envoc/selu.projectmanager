﻿using System.Data.Entity.Migrations;

namespace Selu.ProjectManager.Web.App_Start
{
    public static class DatabaseUpgrader
    {
        public static void PerformUpgrade()
        {
            var configuration = new Data.Migrations.Configuration();
            var migrator = new DbMigrator(configuration);
            migrator.Update();
        }
    }
}