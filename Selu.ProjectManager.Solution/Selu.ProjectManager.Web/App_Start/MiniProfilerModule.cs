﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using MvcMiniProfiler;
using MvcMiniProfiler.MVCHelpers;
using Selu.ProjectManager.Web.App_Start;

[assembly: WebActivator.PreApplicationStartMethod(typeof(MiniProfilerModule), "Start")]
namespace Selu.ProjectManager.Web.App_Start
{
    public class MiniProfilerModule : IHttpModule
    {
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(MiniProfilerModule));
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += (sender, args) =>
                                        {
                                            if (HttpContext.Current.Request.IsLocal)
                                            {
                                                MiniProfiler.Start();
                                            }
                                        };

            context.EndRequest += (sender, args) => MiniProfiler.Stop();

            var copy = ViewEngines.Engines.ToList();
            ViewEngines.Engines.Clear();
            foreach (var item in copy)
            {
                ViewEngines.Engines.Add(new ProfilingViewEngine(item));
            }
        }

        public void Dispose()
        {
        }
    }
}