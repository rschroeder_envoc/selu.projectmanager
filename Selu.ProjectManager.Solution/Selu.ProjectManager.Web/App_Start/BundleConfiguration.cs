﻿using System.Web.Optimization;

namespace Selu.ProjectManager.Web.App_Start
{
    public static class BundleConfiguration
    {
        public static void Configure()
        {
            var jsBundle = new Bundle("~/Js", new JsMinify());
            jsBundle.AddFile("~/Scripts/jquery.validate.js");
            jsBundle.AddFile("~/Scripts/jquery.validate.unobtrusive.js");
            jsBundle.AddFile("~/Scripts/custom.js");
            jsBundle.AddFile("~/Scripts/bootstrap.js");
            jsBundle.AddFile("~/Scripts/jquery-ui-1.8.18.min.js");
            jsBundle.AddFile("~/Scripts/jquery.datatables.js");
            jsBundle.AddFile("~/Scripts/bootstrap.datatables.js");
            jsBundle.AddFile("~/Scripts/bootstrap-dropdown.js");
            BundleTable.Bundles.Add(jsBundle);

            var cssBundle = new Bundle("~/Css", new CssMinify());
            cssBundle.AddFile("~/Content/bootstrap.css");
            cssBundle.AddFile("~/Content/bootstrap-responsive.css");
            cssBundle.AddFile("~/Content/bootstrap-overrides.css");
            cssBundle.AddFile("~/Content/jquery-ui-1.8.18.css");
            cssBundle.AddFile("~/Content/bootstrap-datatables.css");
            BundleTable.Bundles.Add(cssBundle);
        }
    }
}