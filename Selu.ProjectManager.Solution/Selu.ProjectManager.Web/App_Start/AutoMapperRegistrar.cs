﻿using AutoMapper;
using Selu.ProjectManager.Web.App_Start.AutoMapperProfiles;

namespace Selu.ProjectManager.Web.App_Start
{
    public static class AutoMapperRegistrar
    {
        public static void Configure()
        {
            Mapper.Initialize(configuration =>
                {
                    configuration.AddProfile<ProjectProfile>();
                    configuration.AddProfile<ProjectPermissionProfile>();
                    configuration.AddProfile<IssueProfile>();
                    configuration.AddProfile<UserProfile>();
                    configuration.AddProfile<IssueTypeProfile>();
                    configuration.AddProfile<IssuePriorityProfile>();
					configuration.AddProfile<IssueStatusProfile>();
                    configuration.AddProfile<TimeUnitTypeProfile>();
                    configuration.AddProfile<WorkLogProfile>();
                });
        }
    }
}