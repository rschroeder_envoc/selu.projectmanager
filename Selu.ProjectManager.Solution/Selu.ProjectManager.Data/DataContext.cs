﻿using System.Data.Entity;
using Selu.ProjectManager.Data.Configuration;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Data
{
    public class DataContext : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations
                .Add(new ProjectEntityConfiguration())
                .Add(new ProjectPermissionEntityConfiguration())
                .Add(new IssueEntityConfiguration())
                .Add(new UserEntityConfiguration())
                .Add(new IssuePriorityEntityConfiguration())
                .Add(new IssueTypeEntityConfiguration())
                .Add(new TimeUnitTypeEntityConfiguration())
                .Add(new WorkLogEntityConfiguration());
        }
    }
}
