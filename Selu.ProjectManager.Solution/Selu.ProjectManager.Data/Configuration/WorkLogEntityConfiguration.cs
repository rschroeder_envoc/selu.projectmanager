﻿using System.Data.Entity.ModelConfiguration;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Data.Configuration
{
    public class WorkLogEntityConfiguration : EntityTypeConfiguration<WorkLog>
    {
        public WorkLogEntityConfiguration()
        {
            Property(x => x.Description)
                .IsRequired();

            HasRequired(x => x.Issue)
                .WithMany(x => x.WorkLogs)
                .HasForeignKey(x => x.IssueId);
            
            HasRequired(x => x.User)
                .WithMany()
                .HasForeignKey(x => x.UserId);
        }
    }
}
