﻿using System.Data.Entity.ModelConfiguration;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Data.Configuration
{
    public class IssueEntityConfiguration : EntityTypeConfiguration<Issue>
    {
        public IssueEntityConfiguration()
        {
            HasKey(x => x.Id);

            Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.Description)
                .IsRequired();

            Property(x => x.DeadlineDate)
                .IsRequired();

            HasRequired(x => x.Project)
                .WithMany(x => x.Issues)
                .HasForeignKey(x => x.ProjectId);

			HasRequired(x => x.AssignedToUser)
				.WithMany()
				.HasForeignKey(x => x.AssignedToUserId);

            HasRequired(x => x.IssueType)
                .WithMany()
                .HasForeignKey(x => x.IssueTypeId);

            HasRequired(x => x.IssuePriority)
                .WithMany()
                .HasForeignKey(x => x.IssuePriorityId);

			HasRequired(x => x.IssueStatus)
				.WithMany()
				.HasForeignKey(x => x.IssueStatusId);

            HasRequired(x => x.EstimatedTimeUnitType)
                .WithMany()
                .HasForeignKey(x => x.EstimatedTimeUnitTypeId);
        }
    }
}
