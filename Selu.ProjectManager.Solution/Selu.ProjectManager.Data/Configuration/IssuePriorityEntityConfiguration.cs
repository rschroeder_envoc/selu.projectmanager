﻿using System.Data.Entity.ModelConfiguration;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Data.Configuration
{
    public class IssuePriorityEntityConfiguration : EntityTypeConfiguration<IssuePriority>
    {
        public IssuePriorityEntityConfiguration()
        {
            Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(50);
        }
    }
}
