﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Data.Configuration
{
    public class AccessLogEntityConfiguration : EntityTypeConfiguration<AccessLog>
    {
        public AccessLogEntityConfiguration()
        {
            HasKey(x => x.Id);
        }
    }
}
