﻿using System.Data.Entity.ModelConfiguration;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Data.Configuration
{
	public class IssueStatusEntityConfiguration : EntityTypeConfiguration<IssueStatus>
	{
		public IssueStatusEntityConfiguration()
		{
			Property(x => x.Name)
				.IsRequired()
				.HasMaxLength(50);
		}
	}
}
