﻿using System.Data.Entity.ModelConfiguration;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Data.Configuration
{
    public class IssueTypeEntityConfiguration : EntityTypeConfiguration<IssueType>
    {
        public IssueTypeEntityConfiguration()
        {
            Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(50);
        }
    }
}
