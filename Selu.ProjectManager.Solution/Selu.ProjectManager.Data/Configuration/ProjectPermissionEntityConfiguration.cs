using System.Data.Entity.ModelConfiguration;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Data.Configuration
{
    public class ProjectPermissionEntityConfiguration : EntityTypeConfiguration<ProjectPermission>
    {
        public ProjectPermissionEntityConfiguration()
        {
            Property(x => x.Permission).IsRequired().HasMaxLength(10);

            HasRequired(x => x.Project)
                .WithMany(x => x.Permissions)
                .HasForeignKey(x => x.ProjectId);

            HasRequired(x => x.User)
                .WithMany()
                .HasForeignKey(x => x.UserId);
        }
    }
}