﻿using System.Data.Entity.ModelConfiguration;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Data.Configuration
{
    public class TimeUnitTypeEntityConfiguration : EntityTypeConfiguration<TimeUnitType>
    {
        public TimeUnitTypeEntityConfiguration()
        {
            Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(50);
        }
    }
}
