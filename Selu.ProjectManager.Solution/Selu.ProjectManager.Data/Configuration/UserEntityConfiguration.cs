using System.Data.Entity.ModelConfiguration;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Data.Configuration
{
    public class UserEntityConfiguration : EntityTypeConfiguration<User>
    {
        public UserEntityConfiguration()
        {
            HasKey(x => x.Id);

            Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.Email)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.Role)
                .IsRequired();

            Property(x => x.Password)
                .IsRequired();

            HasMany(x => x.AccessLogs)
                .WithRequired(x => x.User)
                .HasForeignKey(x => x.UserId);
        }
    }
}