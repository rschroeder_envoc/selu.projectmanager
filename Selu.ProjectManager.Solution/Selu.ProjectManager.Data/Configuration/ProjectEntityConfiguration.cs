using System.Data.Entity.ModelConfiguration;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Data.Configuration
{
    public class ProjectEntityConfiguration : EntityTypeConfiguration<Project>
    {
        public ProjectEntityConfiguration()
        {
            HasKey(x => x.Id);

            Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(50);
        }
    }
}