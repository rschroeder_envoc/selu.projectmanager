﻿using System;
using System.Data;
using System.Transactions;
using Selu.ProjectManager.Common.Data;
using Selu.ProjectManager.Models;
using IsolationLevel = System.Transactions.IsolationLevel;

namespace Selu.ProjectManager.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext context;
        private TransactionScope transaction;

        public UnitOfWork(DataContext context)
        {
            this.context = context;
        }

        public void Begin()
        {
            if (this.transaction != null)
            {
                throw new InvalidOperationException("The unit of work has already been initialized. Commit or rollback before re-initializing.");
            }
            TransactionOptions options = new TransactionOptions
                                             {
                                                 IsolationLevel = IsolationLevel.ReadCommitted,
                                                 Timeout = TimeSpan.FromMinutes(2.00)
                                             };
            this.transaction = new TransactionScope(TransactionScopeOption.RequiresNew, options);
        }

        public void Commit()
        {
            if (this.transaction == null)
            {
                throw new InvalidOperationException("The unit of work was never initialized");
            }
            SetAuditiableProperties();
            context.SaveChanges();
            transaction.Complete();
            Dispose();
        }

        private void SetAuditiableProperties()
        {
            var entries = context.ChangeTracker.Entries<IAuditable>();
            foreach (var entry in entries)
            {
                if (entry.State.Equals(EntityState.Added))
                {
                    entry.Entity.CreatedDateUtc = DateTime.UtcNow;
                    entry.Entity.UpdatedDateUtc = DateTime.UtcNow;
                }
                else if (entry.State.Equals(EntityState.Modified))
                {
                    entry.Entity.UpdatedDateUtc = DateTime.UtcNow;
                }
            }
        }

        public void Rollback()
        {
            if (transaction == null)
            {
                throw new InvalidOperationException("The unit of work was never initialized");
            }
            Dispose();
        }

        public void Dispose()
        {
            if (transaction == null)
            {
                return;
            }
            transaction.Dispose();
            transaction = null;
            context.Dispose();
        }
    }
}
