// <auto-generated />
namespace Selu.ProjectManager.Data.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    
    public sealed partial class AddedTimeUnitTypes : IMigrationMetadata
    {
        string IMigrationMetadata.Id
        {
            get { return "201202260110127_AddedTimeUnitTypes"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return "H4sIAAAAAAAEAOy9B2AcSZYlJi9tynt/SvVK1+B0oQiAYBMk2JBAEOzBiM3mkuwdaUcjKasqgcplVmVdZhZAzO2dvPfee++999577733ujudTif33/8/XGZkAWz2zkrayZ4hgKrIHz9+fB8/Iv7Hv/cffPx7vFuU6WVeN0W1/Oyj3fHOR2m+nFazYnnx2Ufr9nz74KPf4+g3Th6fzhbv0p807fbQjt5cNp99NG/b1aO7d5vpPF9kzXhRTOuqqc7b8bRa3M1m1d29nZ2Du7s7d3MC8RHBStPHr9bLtljk/Af9eVItp/mqXWflF9UsLxv9nL55zVDTF9kib1bZNP/so9d5uR6/rKufzqftF9kyu8jr8dOszT5Kj8sia7jB+Xsit/MQyH1ku6WOTwnB9vrN9Srnzj/7SLv0G1Gz3yu/Dj6gj6jhKq/b61f5ub56NvsovRu+d7f7on3Newe902/L9t7eR+mLdVlmk5I+OM/KJv8oXX366HVb1fnn+TKvszafvczaNq+XeDdn7JUKj1af3o4QD+/u7IEQd7Plsmqzlqa6h3gHTfxrEH3d1sQ1H6XPinf57Hm+vGjnFtkvsnfmk/vEOl8tC+Ixeqet13lkbJs7PWuOp21xaTt+UlVlni3fG85JnRPdiHnyr9qpAYY/3xQY1XtC+2o1wyx8U+DOmqd5mRPA9x7li+yyuODZ64Fs1nnzUfoqL/nrZl6sRF7G/NXvb3g8fVZXi1cVugi++P3fZPVFTt+/qWLfvq7W9bSDz+O7TpI2yheD+pF0/dxL19O8mdbFSvB7r77p1w/tXFnp5vm5SXiImQDgGwH0si6qmmb8Q4GdNmT0wEuiEFRBVGuC8GGwiObtNzHap3k2K4slq8QPVmD/L9eu34wJ+VnQ0lYJf301bRRxXE0bJX5bhKwwbUDJa9NFyn41gJb7/mshZoTzJuRcuyiC5utNSNo274toVFg3IDzQvot4tNnAAOJtYwN5P4vNmP3Iav+cW+0fadthQO/H0VZR/Iirf8TV/3/g6sCI/Iipf8TU/39g6q+avP4RM//cM/PpIivKH3qvL7OmuapqOy+37PgbSFDAp/7h9/qzojL+f64yjpummhYsOb57Z6PnEJfT5SzdGErLjNsYjWZ4XbbFqiym1PdnH32rN7ohiDaH6iAalDowd0McCeaXSyFRCopjIeYka6bZrE9aosks/IRUYF5DF2UlLbc0bZ0Vy7avL4vltFhl5SbMOy/dUs0CKQu++83TfJUvoSk3zcFt+tVX4t3bXjq0uok0j+96vHQbFhuKjLssEUmNfDCb9dMpXZiM2P9LWa2H/W0m/Rthtt5c3Kpn89L/GxhuIHCNMkg/3fXNMF4vRdaF69Jw/y9mwO4obsUK3xgTdufmVr37L96WGQXFb5wZ49nLzcxzQyrzg5lzc/rTgx9mXP/fyaMbB3MrbvkmeHXjlN0Gi8ElrB+CIhWXkd5p6Y28VozIBc7wWf6u7x6i/eu8Df0MWkx3zmfXmeuxZAiCqRgDoCx+m9fx1iAIYeHbgFHFUQzDcmr7Bnj+XMaghfJ1AzAE+TEgHPx3X/ZmujM86/KnXpvoilqX8W6KCizCbjp7zHtTHODBcFzVVR/h4G49cI8PhobumtyEuG35QcN3UCIE8Jn6mySBY9+NZIh7UEOD6PlQX5scPa+pC8kX0G+ILgNrjEP0uYVRj4xus1n/OvTabMg9iB1FdGuqmXSCtQ32u8d3X3PGTj94fJeaTPNVu87KLyi1Uzbmiy+y1YoSQ+Zv90n6epVNaSgn2681H3i7ZODBXcoHLgTG3Wnjk71ryWxPlI7MLvLOt9Q1YfqsqJsWtm6SIZlyMlv0mt3CEpqeOgaxP4FGeZsX8Lu8RPy1Nvbyi2xJ+NZj9GxtaAeYo+QzGtwCHgmnXfsqtP8mvft6mpVZ3c06kcNxUpXrxXLYNxp+G/+G78snt4fgsmcBFvbT20M6CdcVfHCdr24Ps7u64APtfnd7qF6mLxy0/bgP6/HdztR3Gc1LA2rLjsh3OfdWfK2K6Rvhaob1NXh64L3/t3L007yZ1sVKlix8QMEXt4fn5fR8aBtSfcOwrIPRI5L/xXvC8+PuHsxNQfkw3MDMhVA7X31NmF8tXbw1CH1TUDbcz9M8m5VkRqAYugzgf3N7iP9f0mzflDb//5qOFD/rm9OTMf/ytroy/u7/W/Xlj7j7//3c7QVg3xyLD4Wbt2Xz4fd/xOo/YvWvwephzPyNMPqmdMEt+Hzz6z9i8x+x+ddgc0krfyPszZno92fr+Gv/b2Xn00VWlJ0wQT66PYyXWdNcVXU3hrOf3h4SsoEhFPnk9hB+JJ7/LxDPXp6128T2rp/Yv22eVXOcX/jJVx4/Uqk87kbzrd2kpzT5KCUiXRYzJDxfXzdtvhChfv2LypOyoPG6BiT0xXnetG+qtzklxpGTpVx5WWSNZMnfK5378O7O3t18trjbNLMykswFPY3YxNKZj3+v/Lo7FWa6Ny3yPr4bvui95r2D3j/7qMDoWW19ntPkgH9fZm2b10u0yhnPj9IX67LMJsjOn2dl02PILnhRPNLB8jKrp/OM1vG+yN49z5cX7fyzj+7vvDdMJwwCd1K07w2jI/UCCDJLuZD8vaF1xf0DwXkietsR+oK5kb0imcUfMZcHM8hYhqC3Ftm7O+8NUOW5Q4r3BBLkKj8QjJ+e/PqgOjlJAXReVtkHgvITkF8fuzDt+IHi+P9yXfHhyvBnXd/0Y7kf6ZwfPoP9/5jB4nmxHzHZj5jsm2Gy4aTUj3jsRzz2zfBYP0P0I97yvUTJP32zQF02KoT79Vx9yUp9E5B+JEi3F6TjpqmmRYaIzXcJfv+X0UTK6XKWYp5cokURQE7HJHyJrdZlW6zKYkodfvbRbi91/OVS8E9Biopk5SRrptmsP25CejaEgcTifv/6Sdj7t3pASfjzGrKZlZRMa9qa0mm9VB35RMtpscrKzng77W6pUzAUC7H7zdN8lS+hLIKB3aYjRSrenwXbIelNw3981+OJ27DKUJDSmSpu0Zsu+fT/hwwzQBVt+3PGMkEu5OeaaQYCj86U2Va9qXPf/P+VgeIU0vY/t0zkZ8J+Dhkpmv8aZKigkT+T4Rf/P2SnYfJo858zbhrMYP4QuEqcIrumZhzI7vpXj590mS60wx+lzsfqukSygkZ+46SiKRc3Tb9sehPcha980YOun8dg81e3hCw8H4cu3w32gK9v2YtT1vGe3PeDvWmT4hZdhvLc6zH8Otah3+Lm7jj87HcjH8fA45s+WI9LO9SzDnnqtfHJF/fYA4XTYVPqwfusJ2w9ReW9pZ90lV2I/q2H5jHa0OBckxvQ9OfboSqf/lwP0XH4xmEO2fxhv6iLtvvm53DIccM8OPRb2PEA/ZiA8yjCL74pAgDlHgFMUG3th/3u8V0Rev2A/qQkUXaRf0GWpWz4U7Ja6yVSCPIXLZ0WFw7EY4K5zNm7cEBNm7PleWXsaAcj06STMfgibzNKWWTHdVucZ9OWvp7mTVMsLz5KfzIr1zDDi0k+O1t+uW5X65ZmKV9MyoCDYH439f/4bg/nx1/yWnDzTQyB0CyQdfly+WRdlDOL97NIsmMABOy6Juowly0SdhfXFtKLanlLQEq+p8YdeZMvViUBa75cvs6Q2xnC7WYahhR7/LTILups4VNQPlFMXmfUs9cFdeC/4fqjP4ldZ4t3R/9PAAAA///WnCWXx2IAAA=="; }
        }
    }
}
