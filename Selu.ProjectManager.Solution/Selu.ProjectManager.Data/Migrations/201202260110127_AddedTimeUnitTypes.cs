using System;
using Selu.ProjectManager.Common.Extentions;

namespace Selu.ProjectManager.Data.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddedTimeUnitTypes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "TimeUnitTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        CreateDateUtc = c.DateTime(nullable: false),
                        UpdatedDateUtc = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("Issues", "EstimatedTime", c => c.Double(nullable: false));
            AddColumn("Issues", "EstimatedTimeUnitTypeId", c => c.Int(nullable: false, defaultValue: 1));

            var date = DateTime.UtcNow.ToSqlDateTime();
            Sql("INSERT INTO TimeUnitTypes(Name, CreateDateUtc, UpdatedDateUtc, IsActive, IsDeleted) VALUES('Hours', '" + date + "', '" + date + "', 1, 0), ('Days', '" + date + "', '" + date + "', 1, 0), ('Minutes', '" + date + "', '" + date + "', 1, 0)");

            AddForeignKey("Issues", "EstimatedTimeUnitTypeId", "TimeUnitTypes", "Id", cascadeDelete: true);
            CreateIndex("Issues", "EstimatedTimeUnitTypeId");
        }
        
        public override void Down()
        {
            DropIndex("Issues", new[] { "EstimatedTimeUnitTypeId" });
            DropForeignKey("Issues", "EstimatedTimeUnitTypeId", "TimeUnitTypes");
            DropColumn("Issues", "EstimatedTimeUnitTypeId");
            DropColumn("Issues", "EstimatedTime");
            DropTable("TimeUnitTypes");
        }
    }
}
