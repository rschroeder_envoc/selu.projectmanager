using System;
using Selu.ProjectManager.Common.Extentions;

namespace Selu.ProjectManager.Data.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddedIssuePriorities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "IssuePriorities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        CreateDateUtc = c.DateTime(nullable: false),
                        UpdatedDateUtc = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            Sql("EXEC sp_rename 'IssueTypes.Type', 'Name', 'COLUMN'");

            AddColumn("Issues", "IssuePriorityId", c => c.Int(nullable: false, defaultValue: 2));

            var date = DateTime.UtcNow.ToSqlDateTime();
            Sql("INSERT INTO IssuePriorities(Name, CreateDateUtc, UpdatedDateUtc, IsActive, IsDeleted) VALUES('High', '" + date + "', '" + date + "', 1, 0), ('Normal', '" + date + "', '" + date + "', 1, 0), ('Low', '" + date + "', '" + date + "', 1, 0)");

            AddForeignKey("Issues", "IssuePriorityId", "IssuePriorities", "Id", cascadeDelete: true);
            CreateIndex("Issues", "IssuePriorityId");
        }
        
        public override void Down()
        {
            Sql("EXEC sp_rename 'IssueTypes.Name', 'Type', 'COLUMN'");
            DropIndex("Issues", new[] { "IssuePriorityId" });
            DropForeignKey("Issues", "IssuePriorityId", "IssuePriorities");
            DropColumn("Issues", "IssuePriorityId");
            DropTable("IssuePriorities");
        }
    }
}
