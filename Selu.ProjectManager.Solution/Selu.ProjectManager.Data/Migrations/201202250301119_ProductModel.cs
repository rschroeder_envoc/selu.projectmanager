using System.Data.Entity.Migrations;

namespace Selu.ProjectManager.Data.Migrations
{
    public partial class ProductModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Projects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsActive = c.Boolean(nullable: false),
                        CreateDateUtc = c.DateTime(nullable: false),
                        UpdatedDateUtc = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("Projects");
        }
    }
}
