using System;
using Selu.ProjectManager.Common.Extentions;

namespace Selu.ProjectManager.Data.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddedIssueStatus : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "IssueStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CreatedDateUtc = c.DateTime(nullable: false),
                        UpdatedDateUtc = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

			AddColumn("Issues", "IssueStatusId", c => c.Int(nullable: false, defaultValue: 2));
			
			var date = DateTime.UtcNow.ToSqlDateTime();
			Sql("INSERT INTO IssueStatus(Name, CreatedDateUtc, UpdatedDateUtc, IsActive, IsDeleted) VALUES('Planned', '" + date + "', '" + date + "', 1, 0), ('Being Developed', '" + date + "', '" + date + "', 1, 0), ('Closed', '" + date + "', '" + date + "', 1, 0)");
			
            AddForeignKey("Issues", "IssueStatusId", "IssueStatus", "Id", cascadeDelete: true);
            CreateIndex("Issues", "IssueStatusId");

			Sql("UPDATE Issues SET EstimatedTimeUnitTypeId = 1 WHERE EstimatedTimeUnitTypeId = 2 OR EstimatedTimeUnitTypeId = 3");
			Sql("INSERT INTO TimeUnitTypes(Name, CreatedDateUtc, UpdatedDateUtc, IsActive, IsDeleted) VALUES('Story Points', '" + date + "', '" + date + "', 1, 0)");
        }
        
        public override void Down()
        {
            DropIndex("Issues", new[] { "IssueStatusId" });
            DropForeignKey("Issues", "IssueStatusId", "IssueStatus");
            DropColumn("Issues", "IssueStatusId");
            DropTable("IssueStatus");
        }
    }
}
