namespace Selu.ProjectManager.Data.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddedUsers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Email = c.String(nullable: false, maxLength: 50),
                        Password = c.String(nullable: false),
                        Role = c.String(nullable: false),
                        CreateDateUtc = c.DateTime(nullable: false),
                        UpdatedDateUtc = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            CreateIndex(
                table: "Users",
                column: "Email",
                unique: true,
                name: "UX_Users_Email"
            );
        }

        public override void Down()
        {
            DropIndex("Users", "UX_Users_Email");
            DropTable("Users");
        }
    }
}
