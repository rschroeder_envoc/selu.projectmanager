namespace Selu.ProjectManager.Data.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddedWorkLogs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "WorkLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        IssueId = c.Int(nullable: false),
                        DateWorked = c.DateTime(nullable: false),
                        HoursWorked = c.Double(nullable: false),
                        Description = c.String(nullable: false),
                        CreateDateUtc = c.DateTime(nullable: false),
                        UpdatedDateUtc = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("Issues", t => t.IssueId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.IssueId);
            
        }
        
        public override void Down()
        {
            DropIndex("WorkLogs", new[] { "IssueId" });
            DropIndex("WorkLogs", new[] { "UserId" });
            DropForeignKey("WorkLogs", "IssueId", "Issues");
            DropForeignKey("WorkLogs", "UserId", "Users");
            DropTable("WorkLogs");
        }
    }
}
