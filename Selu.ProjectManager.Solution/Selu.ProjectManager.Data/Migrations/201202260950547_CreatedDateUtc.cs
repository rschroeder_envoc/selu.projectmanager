namespace Selu.ProjectManager.Data.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class CreatedDateUtc : DbMigration
    {
        public override void Up()
        {
            RenameColumn("WorkLogs", "CreateDateUtc", "CreatedDateUtc");
            RenameColumn("Users", "CreateDateUtc", "CreatedDateUtc");
            RenameColumn("Issues", "CreateDateUtc", "CreatedDateUtc");
            RenameColumn("Projects", "CreateDateUtc", "CreatedDateUtc");
            RenameColumn("IssueTypes", "CreateDateUtc", "CreatedDateUtc");
            RenameColumn("IssuePriorities", "CreateDateUtc", "CreatedDateUtc");
            RenameColumn("TimeUnitTypes", "CreateDateUtc", "CreatedDateUtc");
        }
        
        public override void Down()
        {
            RenameColumn("WorkLogs", "CreatedDateUtc", "CreateDateUtc");
            RenameColumn("Users", "CreatedDateUtc", "CreateDateUtc");
            RenameColumn("Issues", "CreatedDateUtc", "CreateDateUtc");
            RenameColumn("Projects", "CreatedDateUtc", "CreateDateUtc");
            RenameColumn("IssueTypes", "CreatedDateUtc", "CreateDateUtc");
            RenameColumn("IssuePriorities", "CreatedDateUtc", "CreateDateUtc");
            RenameColumn("TimeUnitTypes", "CreatedDateUtc", "CreateDateUtc");
        }
    }
}
