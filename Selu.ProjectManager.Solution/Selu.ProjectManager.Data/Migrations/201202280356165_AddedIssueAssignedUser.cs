namespace Selu.ProjectManager.Data.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddedIssueAssignedUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("Issues", "AssignedToUserId", c => c.Int(nullable: false));

        	Sql("UPDATE Issues SET AssignedToUserId = 1");

            AddForeignKey("Issues", "AssignedToUserId", "Users", "Id");
            CreateIndex("Issues", "AssignedToUserId");
        }
        
        public override void Down()
        {
            DropIndex("Issues", new[] { "AssignedToUserId" });
            DropForeignKey("Issues", "AssignedToUserId", "Users");
            DropColumn("Issues", "AssignedToUserId");
        }
    }
}
