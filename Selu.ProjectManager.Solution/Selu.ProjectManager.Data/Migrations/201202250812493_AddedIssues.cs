namespace Selu.ProjectManager.Data.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddedIssues : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Issues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Description = c.String(nullable: false),
                        ProjectId = c.Int(nullable: false),
                        DeadlineDate = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreateDateUtc = c.DateTime(nullable: false),
                        UpdatedDateUtc = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Projects", t => t.ProjectId, cascadeDelete: true)
                .Index(t => t.ProjectId);
            
        }
        
        public override void Down()
        {
            DropIndex("Issues", new[] { "ProjectId" });
            DropForeignKey("Issues", "ProjectId", "Projects");
            DropTable("Issues");
        }
    }
}
