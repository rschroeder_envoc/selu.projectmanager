using System;
using System.Data.Entity.Migrations;
using System.Linq;
using Selu.ProjectManager.Models;

namespace Selu.ProjectManager.Data.Migrations
{
    public sealed class Configuration : DbMigrationsConfiguration<DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DataContext context)
        {
            var users = context.Set<User>();
            if (!users.Any())
            {
                users.Add(new User
                              {
                                  Email = "admin@temp.com",
                                  Name = "Temp Admin",
                                  CreatedDateUtc = DateTime.UtcNow,
                                  UpdatedDateUtc = DateTime.UtcNow,
                                  Role = Roles.Admin,
                                  Password = "AGo7ibycznpt/4FiXjfMTXLSfZIqbXFv2IZErGAOKG4SW7kiJ22sWArgUzw9pscOyA=="  //password
                              });
                context.SaveChanges();
            }
        }
    }
}
