using System;
using Selu.ProjectManager.Common.Extentions;

namespace Selu.ProjectManager.Data.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddedIssueTypes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "IssueTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(nullable: false, maxLength: 50),
                        CreateDateUtc = c.DateTime(nullable: false),
                        UpdatedDateUtc = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            AddColumn("Issues", "IssueTypeId", c => c.Int(nullable: false, defaultValue: 1));

            var date = DateTime.UtcNow.ToSqlDateTime();
            Sql("INSERT INTO IssueTypes(Type, CreateDateUtc, UpdatedDateUtc, IsActive, IsDeleted) VALUES('Feature', '" + date + "', '" + date + "', 1, 0), ('Bug', '" + date + "', '" + date + "', 1, 0)");

            AddForeignKey("Issues", "IssueTypeId", "IssueTypes", "Id", cascadeDelete: true);
            CreateIndex("Issues", "IssueTypeId");
        }
        
        public override void Down()
        {
            DropIndex("Issues", new[] { "IssueTypeId" });
            DropForeignKey("Issues", "IssueTypeId", "IssueTypes");
            DropColumn("Issues", "IssueTypeId");
            DropTable("IssueTypes");
        }
    }
}
