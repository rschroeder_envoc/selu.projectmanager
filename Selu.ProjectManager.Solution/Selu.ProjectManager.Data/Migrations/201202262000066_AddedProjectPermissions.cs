namespace Selu.ProjectManager.Data.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddedProjectPermissions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "ProjectPermissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProjectId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        Permission = c.String(nullable: false, maxLength: 10),
                        CreatedDateUtc = c.DateTime(nullable: false),
                        UpdatedDateUtc = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Projects", t => t.ProjectId, cascadeDelete: true)
                .ForeignKey("Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.ProjectId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropIndex("ProjectPermissions", new[] { "UserId" });
            DropIndex("ProjectPermissions", new[] { "ProjectId" });
            DropForeignKey("ProjectPermissions", "UserId", "Users");
            DropForeignKey("ProjectPermissions", "ProjectId", "Projects");
            DropTable("ProjectPermissions");
        }
    }
}
