﻿namespace Selu.ProjectManager.Common.Extentions
{
    public static class ClassExtentions
    {
        public static bool IsNull(this object obj)
        {
            return obj == null;
        }

        public static bool IsNotNull(this object obj)
        {
            return obj != null;
        }
    }
}
