using System;

namespace Selu.ProjectManager.Common.Data
{
    public interface IUnitOfWork : IDisposable
    {
        void Begin();

        void Commit();

        void Rollback();
    }
}
